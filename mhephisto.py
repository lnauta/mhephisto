"""
mhephisto (My High Energy Physics HISTOgram)

A tool for converting ROOT histgrams (especially TH1 and TH2) to numpy histograms.

Author: lnauta
"""

import matplotlib.pyplot as plt
import numpy as np
import ROOT as r 
from scipy.interpolate import interp1d
import matplotlib as mpl

# settings
mpl.rcParams["text.usetex"] = True
mpl.rcParams["font.family"] = "serif"
mpl.rcParams["font.serif"]  = "cmr10"
mpl.rcParams["xtick.top"] = True
mpl.rcParams["ytick.right"] = True
mpl.rcParams["savefig.bbox"] = "tight"
mpl.rcParams['image.cmap'] = "Blues"
mpl.rcParams['axes.linewidth'] = 1.3
mpl.rcParams['xtick.major.width'] = 1.3
mpl.rcParams['ytick.major.width'] = 1.3
mpl.rcParams['xtick.minor.width'] = 1.
mpl.rcParams['ytick.minor.width'] = 1.
mpl.rcParams['axes.labelsize'] = 'x-large'
mpl.rcParams['xtick.labelsize'] = 'large'
mpl.rcParams['ytick.labelsize'] = 'large'
mpl.rcParams['axes.titlesize'] = 'large'
mpl.rcParams['axes.formatter.use_mathtext'] = True

# set to true if you want to see figures
block=True

# colors
c_el = 'mediumseagreen'
c_mu = 'royalblue'
c_ta = '#ae5afc' # plum used in print # '#ae5afc' used in digital
c_nc = 'darkgray'
c_data = 'black'
c_black = c_data
c_silver = 'darkgray'
c_red = '#de5489'
c_purple = c_ta
c_blue = c_mu
c_green = c_el
c_lightblue = 'dodgerblue'
c_darkgreen = 'seagreen'

#print("Loading mhephisto")

def convertTH1(histogram):
  """
  Convert a TH1 histogram to a set of numbers used in matplotlib plotting

  input: TH1 object from ROOT
  output: [plt.hist, plt.error]
  """
  h = histogram
  a = h.GetXaxis()

  bin_content = np.array([])
  bin_yerr = np.array([])
  bin_lo_edges = np.array([])
  bin_center = np.array([])
  bin_up_edges = np.array([])
  bin_width = np.array([])

  # ROOT histograms go from 1 to n+1. 0 is the underflow bin and n+2 is the overflow bin.
  for i in range(1, int(a.GetNbins())+1):
    bin_content = np.append(bin_content, [h.GetBinContent(i)])
    bin_yerr = np.append(bin_yerr, [h.GetBinError(i)])

    bin_lo_edges = np.append(bin_lo_edges, [a.GetBinLowEdge(i)])
    bin_center = np.append(bin_center, [h.GetBinCenter(i)])
    bin_up_edges = np.append(bin_up_edges, [a.GetBinUpEdge(i)])
    bin_width = np.append(bin_width, [h.GetBinWidth(i)])

  bin_edges = np.union1d(bin_lo_edges, bin_up_edges)
  bin_xerr = [w/2 for w in bin_width]

  return [bin_content, bin_yerr, bin_xerr, bin_center, bin_edges, bin_width]

def getTH1(histogram, ax=None, histtype='step', *args, **kwargs):
  if ax is None:
    ax = plt.gca()

  h = histogram
  a = h.GetXaxis()

  bin_content, bin_yerr, bin_xerr, bin_center, bin_edges, bin_widths = convertTH1(histogram)

  # TODO: Check if the first argument should be bincenters, not arange from 0 to N, what if histo starts at 10 instead of 0?
  #ax.hist(np.arange(h.GetEntries()), bins=bin_edges, weights=bin_content, histtype='step')
  ax.hist(bin_center, bins=bin_edges, weights=bin_content, histtype=histtype, *args, **kwargs)

  return ax


def getTH1err(histogram, ax=None, pop_zero=False, xerr_on=False, *args, **kwargs):
  if ax is None:
    ax = plt.gca()
  if 'fmt' not in kwargs:
    fmt='none'
    

  h = histogram
  a = h.GetXaxis()

  bin_content, bin_yerr, bin_xerr, bin_center, bin_edges, bin_widths = convertTH1(histogram)

  # For some histograms you may want to remove entries that are exactly zero, since ROOT does not draw these, but mpl does.
  if pop_zero:
    zero_value_indices = np.where(bin_content == 0)
    #print(zero_value_indices)
    # doing the below 4 lines in a for loop did not work. python makes a copy perhaps?
    bin_center = np.delete(bin_center, zero_value_indices)
    bin_content = np.delete(bin_content, zero_value_indices)
    bin_xerr = np.delete(bin_xerr, zero_value_indices)
    bin_yerr = np.delete(bin_yerr, zero_value_indices)

  if not xerr_on:
    bin_xerr = None
  ax.errorbar(bin_center, bin_content, xerr=bin_xerr, yerr=bin_yerr, *args, **kwargs)

  return ax


def getPlot(histogram, ax=None, *args, **kwargs):
  if ax is None:
    ax = plt.gca()

  h = histogram

  bin_content, bin_yerr, bin_xerr, bin_center, bin_edges, bin_widths = convertTH1(histogram)

  ax.plot(bin_center, bin_content, *args, **kwargs)

  return ax


def getTH2(uproot_th2, ax=None, zero_to_nan=False, *args, **kwargs):
  h2 = uproot_th2.to_numpy()

  #print(h2)
  if zero_to_nan: 
    h_replaced = np.where(h2[0] == 0, np.nan, h2[0])
    mesh = ax.pcolormesh(h2[1], h2[2], np.swapaxes(h_replaced, 0, 1), rasterized=True, *args, **kwargs)
  else:
    mesh = ax.pcolormesh(h2[1], h2[2], np.swapaxes(h2[0], 0, 1), rasterized=True, *args, **kwargs)

  return ax, mesh


def getScatter(graph):
  """
  graph is a TGraph
  """
  x = []
  y = []
  yerr = []
  for i in range(graph.GetN()):
    x.append(graph.GetPointX(i))
    y.append(graph.GetPointY(i))
    yerr.append(graph.GetErrorY(i))

  return x, y, yerr

def getRooCurve(frame, ax=None, interp=False, *args, **kwargs):
  """
  frame is a RooPlot made by RooPlot::frame() 
  """
  if ax is None:
    ax = plt.gca()

  curve = frame.getCurve()
  n_points = curve.GetN()
  xs = [curve.GetPointX(i) for i in range(2, n_points-3)]
  ys = [curve.GetPointY(i) for i in range(2, n_points-3)]

  if interp:
    print("INFO: Smoothing function from RooFit")
    interp = interp1d(xs, ys, kind='cubic')
    xfine = np.linspace(xs[0], xs[-1], num=len(xs)*2, endpoint=True)
    ax.plot(xfine, interp(xfine)+0.01, *args, **kwargs)
  else:
    ax.plot(xs, ys, *args, **kwargs)

  return ax

  

"""
TF1: Read with pyroot as:

f = ROOT.TFile.Open("file.root")
func = qual.GetFunction("fit")

Get arrays to plot function:
fit_xs = np.arange(-5,5,0.1)
fit_values = np.array([fit(x) for x in fit_xs])
"""
