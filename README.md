MHEPHisto
---------

Mhephisto is My High Energy Physics Histo(gramming) tool, a small tool set used for making nice plots in the High Energy Physics (HEP) community. The plotting tools were made during the writing of my PhD thesis, and now split off from the thesis repository into its own.

In the HEP community most people use [ROOT](http://root.cern.ch) to make plots, which is quite unintuitive and takes a lot of work to make good looking plots. Mhephisto allows through the pyROOT and uproot packages to easily import TH1, TH2, TGraph and RooCurves into matplotlib, where styling becomes much easier.

The package includes my style defined at the top through matplotlibs rcParams. Feel free to use and/or change it.

Requirements
------------
- Standard python3 stack with scientific tools like numpy, matplotlib, pandas, scipy.
- ROOT with python bindings (if `import ROOT` works, you're good)
- uproot v4

Install
-------
Add the path of the file to you pythonpath:
`export PYTHONPATH=/path/to/mhephisto:$PYTHONPATH`
